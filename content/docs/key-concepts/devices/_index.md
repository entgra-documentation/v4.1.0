---
bookCollapseSection: true
weight: 1
---

# Devices 

A device is a physical computing unit capable of achieving one or multiple tasks. Entgra IoT Server enables organizations to enroll, secure, manage, and monitor devices, irrespective of the mobile operator, service provider, or the organization.

## Device Ownership 

In some corporate environments, mobile devices are used to carry out organizational tasks such as email access. These devices are categorized into two main groups based on the ownership:

<ul style="list-style-type:disc;">
<li>  <strong>Bring Your Own Device (BYOD)</strong>: These devices are owned by the employee and managed by the employer. They are subject to policies and conventions enforced by the employer.</li>
<li>  <strong>Corporate Owned, Personally Enabled (COPE)</strong>: These devices are owned and managed by the employer. </li>
</ul>

## Device Types 

Devices are subdivided into two main groups based on the usage: 

**Mobile Devices**

These are handheld devices that are usually used for day-to-day ordinary activities such as making phone calls, sending emails, and setting up alarms. Entgra IoT Server supports managing Android, iOS and Windows mobile devices. 
<ul style="list-style-type:disc;">
<li>For a quick hands-on experience see <a href="../../product-guide/device-management-guide/"> Mobile Device Management</a> and <a href="../../product-guide/app-management/"> App Management </a>.</li>
<li>You can also try enrolling <a href="../../product-guide/enrollment-guide/enroll-android/">Android </a>, <a href="../../product-guide/enrollment-guide/enroll-ios/">iOS</a>, and <a href="../../product-guide/enrollment-guide/enroll-windows/">Windows</a>.</li>
<li>You can try adding operations to <a href="../../product-guide/device-management-guide/android-devices/android-device-operations/">Android</a>, <a href="../../product-guide/device-management-guide/apple-devices/apple-device-operations/">iOS</a>, and <a href="../../product-guide/device-management-guide/windows-devices/windows-device-operations/">Windows</a>.</li>
<li>For more information on creating and applying policies, see <a href="../../product-guide/device-management-guide/android-devices/android-device-policies/">Android</a> and <a href="../../product-guide/device-management-guide/apple-devices/apple-device-policies/">iOS</a>. </li>
<li>To view the list of operations supported for the Android, iOS, and Windows devices, see Supported Operations for Mobile Devices for <a href="../../about-entgra-IoT-server/feature-list/android/">Anroid</a>, <a href="../../about-entgra-IoT-server/feature-list/ios/">iOS</a> and <a href="../../about-entgra-IoT-server/feature-list/windows/">Windows</a>.</li>
</ul>

**IoT Devices**: 

These devices are specifically created to function in a connected environment via the internet. They can collect data via embedded sensors and exchange them with other devices. Entgra IoT Server supports managing Android Sense, Arduino, Raspberry Pi and custom IoT device types. 
<ul style="list-style-type:disc;">
<li>For a quick hands-on experience, see <a target="_blank" href="https://docs.wso2.com/display/IoTS310/Enterprise+IoT+solution">Enterprise IoT solution</a>.</li>
<li>You can also try the available samples for <a target="_blank" href="https://docs.wso2.com/display/IoTS310/Android+Sense">Android Sense</a>, <a target="_blank" href="https://docs.wso2.com/display/IoTS310/Arduino">Arduino</a>, and <a target="_blank" href="https://docs.wso2.com/display/IoTS310/Raspberry+Pi">Raspberry Pi</a>. </li>
<li>If the available IoT device types do not meet your requirement, see <a target="_blank" href="https://docs.wso2.com/display/IoTS310/Creating+a+New+Device+Type">Creating a New Device Type</a>, and <a target="_blank" href="https://docs.wso2.com/display/IoTS310/Device+Manufacturer+Guide">Device Manufacturer Guide</a>.</li>
</ul>

## Device Groups

Entgra IoT Server allows you to <a href="../../product-guide/device-management-guide/manage-enrolled-devices/#grouping-devices">group</a> multiple enrolled devices in order to monitor multiple devices in one go. 