---
bookCollapseSection: true
weight: 1
title: "Upgrades"
---
# Upgrades

## Migrate to New Agent Versions

For making the best of Entgra IoT server, we advise you to upgrade the agent in your devices 
keeping up with our latest fixes to the device agent and server-side synchronization. 


Steps to upgrade your agent :

<ol>
<li>Upload the latest version of the <a target="_blank" href="https://mgt.entgra.net/api/application-mgt/v1.0/artifact/android/agent"> device agent</a> to your <a target="_blank" href="https://www.youtube.com/watch?v=FnNi5ZMEdYU"> app publisher</a> and push the latest version to devices through the <a target="_blank" href="https://www.youtube.com/watch?v=AKVw3tyzJ1w&t"> app store</a>. </li>

<strong>Note:</strong><br>
If you are facing synchronisation issues owing the power optimisations added by certain Android device vendors, we recommend that you send this link [1] to your fleet requesting the users to manually download and install the new version.
<br>
[1] [https://mgt.entgra.net/endpoint-mgt](https://mgt.entgra.net/endpoint-mgt)


<li>No re-enrollment is needed.</li>

More information related to the new features of this release can be found <a target=_blank href="https://entgra-documentation.gitlab.io/v4.1.0/docs/about-entgra-IoT-server/about-this-release/">here</a>.
