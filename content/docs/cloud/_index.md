---
bookCollapseSection: true
weight: 3
title: "Entgra Cloud"
---
# About Entgra Cloud 

## Cloud Enrollment Guide

The Entgra Evaluation Cloud is available for customers registered with Entgra. The Cloud Enrollment Guide is aimed at helping administrators with the types of enrollment options available.

If you are unsure about the type of enrollment that suits you best, going through the [Feature List](../about-entgra-IoT-server/feature-list/)  for your device type might be helpful in deciding the best option for you.

Once you decide on the enrollment type, simply click on the video tutorial of the same following its prerequisites.

Should you require any help along this, feel free to drop us a mail to bizdev-group@entgra.io with your registered mail and we will be in touch. 
