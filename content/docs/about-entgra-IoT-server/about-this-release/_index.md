---
bookCollapseSection: true
weight: 2
title: "About This Release"
---
# About This Release

Entgra IoT Server version [4.1.0](https://entgra-documentation.gitlab.io/v4.1.0/docs/about-entgra-IoT-server/) is the successor of Entgra IoT Server [4.0.0](https://entgra-documentation.gitlab.io/v4.1.0/docs/about-entgra-IoT-server/release-archive/#entgra-iots-400).

## What's New in This Release

Some prominent features and enhancements that this release offers are as follows:

<ul style="list-style-type:disc;">

<li><strong>SIM Card Restriction:</strong> <br>
Restricts the use of different SIM cards by locking the device if an invalid SIM card is present. Here, the device gets locked to the specific SIM that was present at the time of enrollment. Accordingly, if the user changes a corporate SIM, the device will get completely locked and will only be able to be unlocked by changing the SIM back to the original. </li><br>

<li><strong>Geofencing and Alerts enabled at Client-end for Android:</strong><br>
In previous releases, we had geofencing enabled at the server-end. This meant that when a device enters or leaves a marked fence, server-side interaction was required to make a decision on the move. This has now been moved to the client-end enabling real-time decision making while crossing boundaries. </li><br>

<li><strong>Two-way Chat between Agent and Server:</strong><br>
This feature enables users and the server administrator to have chat conversations while on remote sessions. Both admin and user are able to initiate chat conversations from their respective ends.
</li><br>

<li><strong>Endpoint Management App to Replace Device Management App:</strong><br>
Our new Endpoint Management App replaces the earlier Device Management UI with a more user-friendly interface whilst simplifying a lot of administrative tasks.</li><br>

<li><strong>Edit Device Name at Enrollment:</strong><br>
In our previous versions, the name of the device was taken programmatically and identifying the devices at enrollment was difficult for the administrator. With this release, we have enabled new functionality for the user/admin to type a meaningful name for their device which will be visible in the device listing page so that the device can be easily identified.
</li><br>

<li><strong>Enabling Android Agent to Rename Devices:</strong><br>
If a tenant configuration is defined to ask the user to provide a name during enrollment, the user will be prompted to do so.</li><br>

<li><strong>Role Creation and Permission Defining:</strong><br>
Enabling a new configuration to create a defined set of roles and permissions upon tenant creation. If the configuration is enabled it should create defined roles and permissions when creating a new tenant.
</li><br>

<li><strong>Factory Reset Protection for Android:</strong><br>
This feature allows the user to select and change the Google account that is used for factory reset protection. 
</li><br>

<li><strong>Uninstall Apps Outside of the App Store:</strong><br>
Allows removing any app that has been installed outside of the App Store by the EMM administrator.
</li><br>

<li><strong>Convenient App Management:</strong><br>
Enhanced UI functionality to easily uninstall applications installed via the App Store.
</li><br>

</ul>


## Compatible Versions
Entgra IoT Server is compatible with WSO2 Carbon 4.4.17 products.

## Known Issues
For a list of known issues in this release, see <a target="_blank" href="https://gitlab.com/entgra/product-iots/-/issues" >Entgra IoT Server 4.1.0 - Known Issues</a>.

## Fixed Issues/Improvements
In addition to the features and performance improvements, Entgra IoTS 4.0.0 carries with it  over 110 various improvements and bug fixes attesting the stability of this product version. 
Here is the list of <a target="_blank" href="https://gitlab.com/entgra/product-iots/-/issues?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=IoT%204.1.0-RC">Fixed Issues/Improvements</a>. 
