---
bookCollapseSection: true
weight: 2
title: "Setting Up the Databases for Clustering"
---

# Setting Up the Databases for Clustering

The following databases are needed when clustering the IoT Server.

<table border="1">
<thead bgcolor="#DCE0E2"><tr>
<th><font style="Arial" size="2">Database Name</font></th>
<th><font style="Arial" size="2">Description</font></th>
<th><font style="Arial" size="2">Database Script Location</font></th></tr></thead>
<tr>
<td><font style="Tahoma" size="2">
App management database
(WSO2APPM_DB)</font></td>
<td><font style="Tahoma" size="2">This database store the mobile and IoT device application details.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts/apimgt/
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
APIM Database (WSO2AM_DB)</font></td>
<td><font style="Tahoma" size="2">This database stores data related to JAX-RS APIs and OAuth token data.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts/apimgt/
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
CDM core database (DM_DS)</font></td>
<td><font style="Tahoma" size="2">This database stores generic data about devices (such as a unique identifier, device type, ownership type), device enrollment information, device operations, policy management related data, etc.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts/cdm/
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Certificate management database</font></td>
<td><font style="Tahoma" size="2">This database stores the mutual SSL certificate details.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts/certmgt
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Registry database (REG_DB)</font></td>
<td><font style="Tahoma" size="2">This database acts as the registry database and stores governance and config registry data. The registry database must be mounted to all nodes in the cluster.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts<br>
<strong>Example:</strong><br>The mysql.sql script to create the database for MySQL.
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Social database 
(WSO2_SOCIAL_DB)</font></td>
<td><font style="Tahoma" size="2">The database used by the WSO2 App Manager Store to gather the likes, comments, and rating of an application.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts/social
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Storage database</font></td>
<td><font style="Tahoma" size="2">The database is used by the WSO2 App Manager to store web application data, such as URI templates, subscriptions, and more.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts/storage
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
User manager database (UM_DB)</font></td>
<td><font style="Tahoma" size="2">This database stores the user details and user permission related details.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts 
<br><strong>Example:</strong><br> The mysql.sql script to create the database for MySQL.
</font></td>
</tr>
</table>
The following databases are related to plugins. These enable you to keep the data that is essential for these devices to work (such as APNS related keys) and this data is not available in the CDM core database.

<table border="1">
<thead bgcolor="#DCE0E2"><tr>
<th><font style="Arial" size="2">Database Name</font></th>
<th><font style="Arial" size="2">Description</font></th>
<th><font style="Arial" size="2">Database Script Location</font></th></tr></thead>
<tr>
<td><font style="Tahoma" size="2">
iOS database (MobileIOSDM_DS)</font></td>
<td><font style="Tahoma" size="2">Stores the iOS related the data.
If you have not configured WSO2 IoT Server for iOS, you won't have the database scripts in the given location. For more information on configuring WSO2 IoT Server for iOS, see <a target="_blank" href="https://docs.wso2.com/display/IoTS310/iOS+Configurations"> iOS Configurations</a>.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/ios
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Android database (MobileAndroidDM_DS)</font></td>
<td><font style="Tahoma" size="2">Stores the Android related data.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/android
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Windows database (MobileWindowsDM_DS)</font></td>
<td><font style="Tahoma" size="2">Stores the Microsoft Windows related data.
</font></td>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/dbscripts/cdm/plugins/windows
</font></td>
</tr>
</table>
To change the datasource configurations, please change the following files.
<br><br>
<strong>NOTE:</strong> Make sure to add the relevant JDBC library to the  <I>&lt;IOTS_HOME&gt;/lib</I> directory. For example, add the <I>mysql-connector-java-{version}.jar</I> file to the  <I>&lt;IOTS_HOME&gt;/lib</I> directory when using the MySQL database.
<br><br>
<table border="1">
<thead bgcolor="#DCE0E2"><tr>
<th><font style="Arial" size="2">Files to Change</font></th>
<th><font style="Arial" size="2">Datasource</font></th></thead>
<tr>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/repository/conf/datasources/master-datasources.xml
</font></td>
<td><font style="Tahoma" size="2">This file must include the datasource configuration for the following databases:
<ul style="list-style-type:disc;">
<li>APIM database - refer <strong>note 1</strong> below for sample database configuration for the API Manager database.</li>
<li>Registry database</li>
<li>User management database - refer <strong>note 2</strong> below for sample database configuration for for the user manager, registry and app manager databases.</li>
<li>App Manager database</li>
<li>Store database</li>
<li>Social database</li>
</ul>
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/repository/conf/datasources/cdm-datasources.xml
</font></td>
<td><font style="Tahoma" size="2">This file must include the datasource configuration for the CDM core database. - refer <strong>note 3</strong> below for sample database configuration that can be used for the CDM, Android, iOS and Windows databases.
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/repository/conf/datasources/android-datasources.xml
</font></td>
<td><font style="Tahoma" size="2">This file must include the datasource configuration for the Android plugin database.
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">&lt;IOTS_HOME&gt;/repository/conf/datasources/ios-datasources.xml
</font></td>
<td><font style="Tahoma" size="2">This file must include the datasource configuration for the iOS plugin database.
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2"><PRODUCT_HOME>/repository/conf/datasources/windows-datasources.xml
</font></td>
<td><font style="Tahoma" size="2">This file must include the datasource configuration for the Windows Plugin database.
</font></td>
</tr>
</table>

<strong>Note 1:</strong> Sample database configuration for the API Manager database.<br>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
&lt;datasource&gt;
   &lt;name&gt;WSO2AM_DB&lt;/name&gt;
   &lt;descriptiong&gt;The datasource used for API Manager database&lt;/descriptiong&gt;
   &lt;jndiConfig&gt;
      &lt;name&gt;jdbc/WSO2AM_DB&lt;/name&gt;
   &lt;/jndiConfigg&gt;
   &lt;definition type="RDBMS"&gt;
      &lt;configuration&gt;
         &lt;url&gt;jdbc:mysql://{hostname}:{port}/apim?autoReconnect=true&amp;relaxAutoCommit=true&amp;zeroDateTimeBehavior=convertToNull</url>
         &lt;username&gt;root&lt;/username&gt;
        &lt;password&gt;root&lt;/password&gt;
        &lt;driverClassName&gt;com.mysql.jdbc.Driver&lt;/driverClassName&gt;
         &lt;maxActive&gt;50&lt;/maxActive&gt;
         &lt;maxWait&gt;60000&lt;/maxWait&gt;
         &lt;testOnBorrow&gt;true&lt;/testOnBorrow&gt;
       &lt;validationQuery&gt;SELECT 1&lt;/validationQuery&gt;
        &lt;validationInterval&gt;30000&lt;/validationInterval&gt;
      &lt;/configuration&gt;
   &lt;/definition&gt;
&lt;/datasource&gt;
</code>
</pre>
</font>
</td>
</tr>
</table><br>

<strong>Note 2:</strong> Sample database configuration for for the user manager, registry and app manager databases.<br>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
&lt;datasource&gt;
   &lt;name&gt;WSO2UM_DB&lt;/name&gt;
   &lt;description&gt;The datasource used for User Manager database&lt;/description&gt;
   &lt;jndiConfig&gt;
      &lt;name&gt;jdbc/WSO2UM_DB&lt;/name&gt;
   &lt;/jndiConfig&gt;
   &lt;definition type="RDBMS"&gt;
      &lt;configuration&gt;
         &lt;url&gt;jdbc:mysql://{hostname}:{port}/userdb?autoReconnect=true&amp;relaxAutoCommit=true&lt;/url&gt;
         &lt;username&gt;root&lt;/username&gt;
         &lt;password&gt;root&lt;/password&gt;
         &lt;driverClassName&gt;com.mysql.jdbc.Driver&lt;/driverClassName&gt;
         &lt;maxActive&gt;50&lt;/maxActive&gt;
         &lt;maxWait&gt;60000&lt;/maxWait&gt;
        &lt;testOnBorrowtrue&gt;&lt;/testOnBorrow&gt;
         &lt;validationQuery&gt;SELECT 1&lt;/validationQuery&gt;
         &lt;validationInterval&gt;30000&lt;/validationInterval&gt;
      &lt;/configuration&gt;
   &lt;/definition&gt;
&lt;/datasource&gt;
</code>
</pre>
</font>
</td>
</tr>
</table>

<strong>Note 3:</strong> Sample database configuration for for the user manager, registry and app manager databases.<br>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
&lt;datasources&gt;
   &lt;datasource&gt;
      &lt;name&gt;DM_DS&lt;/name&gt;
      &lt;description&gt;The datasource used for CDM&lt;/description&gt;
      &lt;jndiConfig&gt;
         &lt;name>jdbc/DM_DS&lt;/name&gt;
      &lt;/jndiConfig&gt;
      &lt;definition type="RDBMS"&gt;
         &lt;configuration&gt;
            &lt;url&gt;jdbc:mysql://{localhost}:3306/cdm?autoReconnect=true&amp;relaxAutoCommit=true</url>
            &lt;username&gt;root&lt;/username&gt;
            &lt;password&gt;root&lt;/password&gt;
            &lt;driverClassName&gt;com.mysql.jdbc.Driver&lt;/driverClassName&gt;
            &lt;maxActive&gt;50&lt;/maxActive&gt;
            &lt;maxWait&gt;60000&lt;/maxWait&gt;
            &lt;testOnBorrow&gt;true&lt;/testOnBorrow&gt;
            &lt;validationQuery&gt;SELECT 1&lt;/validationQuery&gt;
            &lt;validationInterval&gt;30000&lt;/validationInterval&gt;
         &lt;/configuration&gt;
      &lt;/definition&gt;
   &lt;/datasource&gt;
&lt;/datasource&gt;
</code>
</pre>
</font>
</td></tr></table>

```javascript
upstream mgt.iots310.wso2.com {
        ip_hash;
        server 192.168.57.124:9763;
}
 
server {
        listen 80;
        server_name mgt.iots310.wso2.com;
        client_max_body_size 100M;
        location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass http://mgt.iots310.wso2.com;
 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
 
 
 
upstream ssl.mgt.iots310.wso2.com {
    ip_hash;
    server 192.168.57.124:9443;
 
}
 
server {
listen 443;
    server_name mgt.iots310.wso2.com;
    ssl on;
    ssl_certificate /opt/keys/star_wso2_com.crt;
    ssl_certificate_key /opt/keys/iots310_wso2_com.key;
 client_max_body_size 100M;
    location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass https://ssl.mgt.iots310.wso2.com;
 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
```
