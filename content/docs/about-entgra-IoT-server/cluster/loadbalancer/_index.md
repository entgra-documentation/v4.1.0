---
bookCollapseSection: true
weight: 1
title: "Configuring the Load Balancer"
---

# Configuring the Load Balancer

This section provides instructions on how to configure Nginx as the load balancer. You can use any load balancer for your setup.

The location of the file varies depending on how you installed the software on your machine. For many distributions, the file is located at _/etc/nginx/nginx.conf_. If it does not exist there, it may also be at _/usr/local/nginx/conf/nginx.conf_ or _/usr/local/etc/nginx/nginx.conf_. You can create separate files inside the _conf.d_ directory for each configuration. Three different configuration files are used for the Manager, Key Manager and Worker nodes in the example provided in this page.

<strong>Before You Begin:</strong>
<br>
You need to have a signed SSL certificate before starting. When generating the certificate make sure to add the following four URLs as Server Name Indications (SNI).

<table width="320 px" border="1" bgcolor="#D9DEE1">
<tr>
<td><font style="Tahoma" size="2">Worker</td>
<td><font style="Tahoma" size="2">iots310.wso2.com</td>
</tr>
<tr>
<td></td><td><font style="Tahoma" size="2">gateway.iots310.wso2.com</td>
</tr>
<tr>
<td><font style="Tahoma" size="2">Manager</td>
<td><font style="Tahoma" size="2">mgt.iots310.wso2.com</td>
</tr>
<tr>
<td><font style="Tahoma" size="2">Key Manager</td>
<td><font style="Tahoma" size="2">keymgt.iots310.wso2.com
</td></tr>
</table>
<ol>
<li>Create a file named <I>mgt.conf in the /nginx/conf.d</I> directory and add the following to it. This will be used by the Manager node to load balance.

<br>
<table width="600 px" border="1"><tr><td bgcolor="#D9DEE1"><font style="Tahoma" size="2">
<pre> <code>
upstream mgt.iots310.wso2.com {
        ip_hash;
        server 192.168.57.124:9763;
}
 server {
        listen 80;
        server_name mgt.iots310.wso2.com;
        client_max_body_size 100M;
        location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass http://mgt.iots310.wso2.com; 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
  
upstream ssl.mgt.iots310.wso2.com {
    ip_hash;
    server 192.168.57.124:9443; 
}
 server {
listen 443;
    server_name mgt.iots310.wso2.com;
    ssl on;
    ssl_certificate /opt/keys/star_wso2_com.crt;
    ssl_certificate_key /opt/keys/iots310_wso2_com.key;
 client_max_body_size 100M;
    location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass https://ssl.mgt.iots310.wso2.com; 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
</pre></code></font></td></tr></table>


<li>Create a file named <I>wkr.conf</I> in the <I>/nginx/conf.d</I> directory and add the following to it. This will be used by the first worker node to load balance.</li>
<table width="500 px" border="1"><tr><td bgcolor="#D9DEE1"><font style="Tahoma" size="2">
<pre><code>
upstream iots310.wso2.com {
        ip_hash;
        server 192.168.57.125:9763;
        server 192.168.57.126:9763;
}
 
server {
        listen 80;
        server_name iots310.wso2.com;
        location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass http://iots310.wso2.com;
 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
 
 
 
upstream ssl.iots310.wso2.com {
    ip_hash;
    server 192.168.57.125:9443;
    server 192.168.57.126:9443;
}
 
server {
listen 443;
    server_name iots310.wso2.com;
    ssl on;
    ssl_certificate /opt/keys/star_wso2_com.crt;
    ssl_certificate_key /opt/keys/iots310_wso2_com.key;
    location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass https://ssl.iots310.wso2.com;
 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
</code></pre></font></td></tr></table>

<br>
<li>Create a file named <I>gateway.conf</I> in the <I>/nginx/conf.d</I> directory and add the following to it. This will be used by the gateway worker node to load balance.</li>
<table width="500 px" border="1"><tr><td bgcolor="#D9DEE1"><font style="Tahoma" size="2">
<pre><code>
upstream gateway.iots310.wso2.com {
        ip_hash;
        server 192.168.57.125:8280;
        server 192.168.57.126:8280;
}
 
server {
        listen 80;
        server_name gateway.iots310.wso2.com;
        location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass http://gateway.iots310.wso2.com;
 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
 
 
 
upstream ssl.gateway.iots310.wso2.com {
    ip_hash;
    server 192.168.57.125:8243;
    server 192.168.57.126:8243;
}
 
server {
listen 443;
    server_name gateway.iots310.wso2.com;
    ssl on;
    ssl_certificate /opt/keys/star_wso2_com.crt;
    ssl_certificate_key /opt/keys/iots310_wso2_com.key;
    location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass https://ssl.gateway.iots310.wso2.com;
 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
</code></pre></font></td></tr></table>

<li>Create a file named <I>keymgt.conf</I> in the <I>/nginx/conf.d</I> directory and add the following to it. This will be used by the key manager node to load balance.</li>
<table width="500 px" border="1"><tr><td bgcolor="#D9DEE1"><font style="Tahoma" size="2">
<pre><code>
upstream keymgt.iots310.wso2.com {
        ip_hash;
        server 192.168.57.127:9763;
}
 
server {
        listen 80;
        server_name keymgt.iots310.wso2.com;
        location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass http://keymgt.iots310.wso2.com;
 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
 
upstream ssl.keymgt.iots310.wso2.com {
    ip_hash;
    server 192.168.57.127:9443;
 
}
 
server {
listen 443;
    server_name keymgt.iots310.wso2.com;
    ssl on;
    ssl_certificate /opt/keys/star_wso2_com.crt;
    ssl_certificate_key /opt/keys/iots310_wso2_com.key;
    location / {
               proxy_set_header X-Forwarded-Host $host;
               proxy_set_header X-Forwarded-Server $host;
               proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
               proxy_set_header Host $http_host;
               proxy_read_timeout 5m;
               proxy_send_timeout 5m;
               proxy_pass https://ssl.keymgt.iots310.wso2.com;
 
               proxy_http_version 1.1;
               proxy_set_header Upgrade $http_upgrade;
               proxy_set_header Connection "upgrade";
        }
}
</code></pre></font></td></tr></table>

</ol>
