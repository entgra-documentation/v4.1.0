---
bookCollapseSection: true
weight: 5
title: "Configuring the Manager Node"
---

# Configuring the Manager Node

Throughout this guide, you have configured _mgt.iots310.wso2.com_ as the manager node.

<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
Before you begin:
<ul style="list-style-type:disc;">
<li>Mount the registry as explained <a href="../registry/">here</a>.</li>
<li>Configure the following databases for the Key Manager in the <I>&lt;IOTS_HOME&gt;/conf/datasources/master-datasources.xml</I> file.</li>
For more information, see <a href="../databases/">Setting Up the Databases for Clustering</a>.</li>
<ul style="list-style-type:circle;">
<li>Registry Database</li>
<li>User manager database</li>
<li>APIM Database</li>
<li>App manager database and include the social and storage database schemas to the same database.</li>
<li>CDM database and include the certificate management, android, iOS and windows database schemas to the same database.</li>
</ul></ul>
</font></td></tr></table>
Let's start configuring the Manager node.
<ol>
<li>Configure the <I>HostName</I> and  <I>MgtHostName</I> properties in the <I>&lt;IOTS_HOME&gt;/conf/carbon.xml</I> file as shown below.</li>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
&lt;HostName&gt;iots310.wso2.com&lt;/HostName&gt;<br>
&lt;MgtHostName&gt;mgt.iots310.wso2.com&lt;/MgtHostName&gt;
</font></td></tr></table>
<li>Configure the <I>&lt;IOTS_HOME&gt;/bin/iotserver.sh</I> file as shown below:</li>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
-Diot.manager.host="mgt.iots310.wso2.com" \<br>
-Diot.manager.https.port="443" \<br>
-Diot.core.host="iots310.wso2.com" \<br>
-Diot.core.https.port="443" \<br>
-Diot.keymanager.host="keymgt.iots310.wso2.com" \<br>
-Diot.keymanager.https.port="443" \<br>
-Diot.gateway.host="gateway.iots310.wso2.com" \<br>
-Diot.gateway.https.port="443" \<br>
-Diot.gateway.http.port="80" \<br>
-Diot.gateway.carbon.https.port="443" \<br>
-Diot.gateway.carbon.http.port="80" \<br>
-Diot.apimpublisher.host="gateway.iots310.wso2.com" \<br>
-Diot.apimpublisher.https.port="443" \<br>
-Diot.apimstore.host="gateway.iots310.wso2.com" \<br>
-Diot.apimstore.https.port="443" \<br>
</font></td></tr></table>
<li>The publisher and store of the app manager run on manager node. Configuring the app manager:
<ol style="list-style-type:disc;">
<li>Configure the following properties in the  <I>&lt;IOTS_HOME&gt;/repository/deployment/server/jaggeryapps/store/config/store.json</I> file for SSO by replacing  <I>https://localhost:9443</I> with <I>https://keymgt.iots310.wso2.com.</I></li>
<ul style="list-style-type:circle;">
<li>identityProviderURL</li>
<li>storeAcs</li>
</ul>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
"ssoConfiguration":{
      "enabled":true,
      "issuer":"store",
      "identityProviderURL":"https://keymgt.iots310.wso2.com/samlsso",
      "keyStorePassword":"wso2carbon",
      "identityAlias":"wso2carbon",
      "responseSigningEnabled":"true",
      "storeAcs":"https://mgt.iots310.wso2.com/store/acs",
      "keyStoreName":"/repository/resources/security/wso2carbon.jks",
      "validateAssertionValidityPeriod":true,
      "validateAudienceRestriction":true,
      "assertionSigningEnabled":true
},
</pre>
</code>
</font></td></tr></table>
<li>Configure the following properties in the  <I>&lt;IOTS_HOME&gt;/repository/deployment/server/jaggeryapps/publisher/config/publisher.json</I> file for SSO by replacing  <I>https://localhost:9443</I> with <I>https://keymgt.iots310.wso2.com.</I></li>
<ul style="list-style-type:circle;">
<li>identityProviderURL</li>
<li>publishereAcs</li>
</ul>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
"ssoConfiguration":{
      "enabled":true,
      "issuer":"publisher",
      "identityProviderURL":"https://keymgt.iots310.wso2.com/samlsso",
      "keyStorePassword":"wso2carbon",
      "identityAlias":"wso2carbon",
      "responseSigningEnabled":"true",
      "publisherAcs":"https://mgt.iots310.wso2.com/publisher/sso",
      "keyStoreName":"/repository/resources/security/wso2carbon.jks",
      "validateAssertionValidityPeriod":true,
      "validateAudienceRestriction":true,
      "assertionSigningEnabled":true
 }
</pre>
</code>
</font></td></tr></table>
<li>Configure the AppDownloadURLHost property in the <I>&lt;IOTS_HOME&gt;/conf/app-manager.xml</I> to point to  <I>http://mgt.iots310.wso2.com</I> .
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
&lt;Config name="AppDownloadURLHost"&gt;http://mgt.iots310.wso2.com&lt;/Config&gt;
</font></td></tr></table>
</li>
<li>Configure the following properties in the  <I>&lt;IOTS_HOME&gt;/repository/deployment/server/jaggeryapps/api-store/site/conf/site.json</I> file for SSO by replacing  <I>https://localhost:9443</I>  with  <I>https://keymgt.iots310.wso2.com</I>.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
"ssoConfiguration":{
      "enabled":"true",
      "issuer":"API_STORE",
      "identityProviderURL":"https://keymgt.iots310.wso2.com/samlsso",
      "keyStorePassword":"",
      "identityAlias":"",
      "responseSigningEnabled":"true",
      "assertionSigningEnabled":"true",
      "keyStoreName":"",
      "passive":"false",
      "signRequests":"true",
      "assertionEncryptionEnabled":"false"
},
</pre>
</code>
</font></td></tr></table>
</li>
<li>Configure the <I>&lt;IOTS_HOME&gt;/repository/deployment/server/jaggeryapps/android-web-agent/app/conf/config.json</I> file to update the Android agent download URL.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
"generalConfig":{
      "host":"https://mgt.iots310.wso2.com",
      "companyName":"WSO2 IoT Server",
      "browserTitle":"WSO2 IoT Server",
      "copyrightText":"\u00A9 %date-year%, WSO2 Inc. (http://www.wso2.org) All Rights Reserved."
},
</pre>
</code>
</font></td></tr></table>
</li>
<li>Configure the <I>&lt;IOTS_HOME&gt;/repository/deployment/server/jaggeryapps/devicemgt/app/conf/config.json</I> file to update the URL of the QR code, which will be used to enroll a device by scanning the QR code.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
"generalConfig":{
      "host":"https://mgt.iots310.wso2.com",
      "companyName":"WSO2 Carbon Device Manager",
      "browserTitle":"WSO2 Device Manager",
      "copyrightPrefix":"\u00A9 %date-year%, ",
      "copyrightOwner":"WSO2 Inc.",
      "copyrightOwnersSite":"http://www.wso2.org",
      "copyrightSuffix":" All Rights Reserved."
},
</pre>
</code>
</font></td></tr></table>
</li>
<li>Start the core profile of WSO2 IoT Server.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
cd &lt;IOTS_HOME&gt;/bin<br>
./iot-server.sh
</font></td></tr></table>
</li>
</ol>
<li>Optionally, enable the device status monitoring task on the manager node and disable it on the other nodes. Open the <I>&lt;IOTS_HOME&gt;/conf/cdm-config.xml</I> file and make sure the <I>DeviceStatusTaskConfig</I> is enabled. This configuration is enabled by default. For more information, see <a target="_blank" href="https://docs.wso2.com/display/IoTS310/Monitoring+the+Device+Status"> Monitoring the Device Status</a>.
In a clustered environment make sure to enable this task only in the manager node and not the worker nodes. Else, the server crashes when the worker nodes start pushing notifications along with the manager node. </li><br>
<li>Optionally, open the <I>&lt;IOTS_HOME&gt;/conf/cdm-config.xml</I> file and make sure the <I>SchedulerTaskEnabled</I> that is under <I>PushNotificationConfiguration</I> is enabled. This configuration is enabled by default. For more information, see Scheduling the Push Notification Task.
In a clustered environment make sure to enable this task only in the manager node and not the worker nodes. Else, the server crashes when the worker nodes start pushing notifications along with the manager node. </li>
</ol>