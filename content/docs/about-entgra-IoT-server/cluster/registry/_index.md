---
bookCollapseSection: true
weight: 3
title: "Mounting the Registry"
---

# Mounting the Registry

A registry is a virtual directory based repository system. It can be federated among multiple databases, which is referred to as Registry mounting. All WSO2 servers support registry mounting. There are three types of registry-repositories.


<table border="1">
<tr>
<th bgcolor="#D9DEE1"><font style="Arial" size="2">Local</font></th>
<td><font style="Arial" size="2">Stores the data related to the local instances.</td></tr>
<tr><th bgcolor="#D9DEE1"><font style="Arial" size="2">Config</th>
<td><font style="Arial" size="2">Contains product specific configurations that are shared across multiple instances of the same product.</td></tr>
<tr><th bgcolor="#D9DEE1"><font style="Arial" size="2">Governace</th>
<td><font style="Arial" size="2">Contains data and configurations that are shared across all the products in the platform.</td></tr>
</table>

For more information on sharing registry spaces across multiple product instances, see <a target="_blank" href="https://wso2.com/library/tutorials/2010/04/sharing-registry-space-across-multiple-product-instances/">here</a>.

See <a target="_blank" href="https://docs.wso2.com/display/Governance510/Remote+Instance+and+Mount+Configuration+Details">Remote Instance and Mount Configuration Details</a> for more information on registry mounting and why it is useful. These must be done on all nodes. 
<br>Follow the steps given below:
<ol>
<li>Add the following to the <I>&lt;IOTS_HOME&gt;/conf/ datasources/master-datasources.xml</I> file to configure the Key Manager registry mounting.</li>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
&lt;dbConfig name="mounted_registry"&gt;
   &lt;dataSource&gt;jdbc/WSO2REG_DB&lt;/dataSource&gt;
&lt;/dbConfig&gt;
  
&lt;remoteInstance url="https://localhost:9443/registry"&gt;
   &lt;id&gt;instanceid&lt;/id&gt;
   &lt;dbConfig&gt;mounted_registry&lt;/dbConfig&gt;
   &lt;readOnly&gt;false&lt;/readOnly&gt;
   &lt;enableCache&gt;true&lt;/enableCache&gt;
   &lt;registryRoot&gt;/&lt;/registryRoot&gt;
   &lt;cacheId&gt;root@jdbc:mysql://192.168.57.123:3306/govreg&lt;/cacheId&gt;
&lt;/remoteInstance&gt;
  
&lt;mount path="/_system/config" overwrite="true"&gt;
   &lt;instanceId&gt;instanceid&lt;/instanceId&gt;
   &lt;targetPath&gt;/_system/config/iot&lt;/targetPath&gt;
&lt;/mount&gt;
&lt;mount path="/_system/governance" overwrite="true"&gt;
   &lt;instanceId&gt;instanceid&lt;/instanceId&gt;
   &lt;targetPath&gt;/_system/governance&lt;/targetPath&gt;
&lt;/mount&gt;
</code>
</pre>
</td>
</tr></table>

<li>Add the following configurations to the <I>&lt;IOTS_HOME&gt;/conf/registry.xml</I> file to configure the Worker and Manager registry mounting.</li>
<table border="1" bgcolor="#D9DEE1">
<tr><td><font style="Arial" size="2">

<pre>
<code>
&lt;dbConfig name="mounted_registry"&gt;
   &lt;dataSource&gt;jdbc/WSO2REG_DB&lt;/dataSource&gt;
&lt;/dbConfig &gt;
 
&lt;remoteInstance url="https://localhost:9443/registry"&gt;
   &lt;id&gt;instanceid&lt;/id&gt;
   &lt;dbConfig&gt;mounted_registry&lt;/dbConfig&gt;
   &lt;readOnly&gt;false&lt;/readOnly&gt;
   &lt;enableCache&gt;true&lt;/enableCache&gt;
   &lt;registryRoot&gt;/&lt;/registryRoot&gt;
   &lt;cacheId&gt;root@jdbc:mysql://192.168.57.123:3306/govreg&lt;/cacheId&gt;
&lt;/remoteInstance&gt;
 
&lt;mount path="/_system/config" overwrite="true"&gt;
   &lt;instanceId&gt;instanceid&lt;/instanceId&gt;
   &lt;targetPath&gt;/_system/config/iot&lt;/targetPath&gt;
&lt;/mount&gt;
&lt;mount path="/_system/governance" overwrite="true"&gt;
   &lt;instanceId&gt;instanceid&lt;/instanceId&gt;
   &lt;targetPath&gt;/_system/governance&lt;/targetPath&gt;
&lt;/mount&gt;
</code>
</pre>
</td>
</tr></table>
</ol>

Next, let's Configure the Key Manager Node.
