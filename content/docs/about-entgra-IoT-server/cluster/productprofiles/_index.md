---
bookCollapseSection: true
weight: 8
title: "Product Profiles"
---

# Product Profiles

When a WSO2 product starts, it starts all components, features and related artifacts bundled with it. The main functionality of WSO2 IoT Server can be divided into six different profiles that each contain a bundle of specific components, features, and artifacts related to the profile. This section guides you on how to create a profile distribution in order to support a distributed setup. 

By default, each profile is started on a different port number. The following table lists out the profiles available in the WSO2 IoT Server and their respective port numbers.

<table border="1" >
<thead bgcolor="#9bc9f1"><tr>
<th><font style="Arial" size="2">Profile</font></th>
<th><font style="Arial" size="2">Port Numbers</font></th>
<th><font style="Arial" size="2">Description</font></th></tr></thead>
<tr>
<td><font style="Tahoma" size="2">
Device Manager Backend</font></td>
<td><font style="Tahoma" size="2">9444/9763</font></td>
<td><font style="Tahoma" size="2">This profile consists of the core services and device types that are used by the WSO2 IoT core framework and handles external traffic for the backend services. The Device Manager Backend profile does not consist of any Graphical User Interface (GUI) applications that need to be used directly by the end user./dbscripts/appmgt
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Device Manager Backend</font></td>
<td><font style="Tahoma" size="2">9444/9763</font></td>
<td><font style="Tahoma" size="2">This profile consists of the core services and device types that are used by the WSO2 IoT core framework and handles external traffic for the backend services. The Device Manager Backend profile does not consist of any Graphical User Interface (GUI) applications that need to be used directly by the end user./dbscripts/appmgt
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Device Manager</font></td>
<td><font style="Tahoma" size="2">9443/9763</font></td>
<td><font style="Tahoma" size="2">This profile mainly consists of the GUI for all applications that facilitate device management. These applications include the following:
<ul style="list-style-type:disc;">
<li>Device management application</li>
<li>Application manager</li>
<li>Publisher and store</li>
<li>API store</li>
<li>Dashboard Portal.</li>
</ul>
Additionally, there are some exposed REST APIs for the publisher and store in WSO2 App Manager and WSO2 API Manager. These APIs can be called remotely and some actions, which can also be performed through the GUI applications, can be performed through the API itself.
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Key Manager</font></td>
<td><font style="Tahoma" size="2">9447/9767</font></td>
<td><font style="Tahoma" size="2">This profile acts as both a key manager, and an identity provider. The user operations done in the Device Manager profile are authenticated via this profile.
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
IoT Analytics</font></td>
<td><font style="Tahoma" size="2">9445/9764</font></td>
<td><font style="Tahoma" size="2">This profile receives various device information and analytics (real-time and batch analytics) on the events that are received. The events can be published to the analytics server directly or it can be pushed via a broker profile.
</font></td>
</tr>
<tr>
<td><font style="Tahoma" size="2">
Broker</font></td>
<td><font style="Tahoma" size="2">1886</font></td>
<td><font style="Tahoma" size="2">This profile acts as the MQTT broker that can be published and subscribed to from profiles such as the device backend profile and the analytics profile.  
</font></td>
</tr>
</table>

## Creating Profile Distributions

The distributions for the profiles are created by executing a script from the WSO2 IoT distribution. Follow the steps given below to create the respective profiles:

<ol>
<li>Download <a target="_blank" href="https://wso2.com/iot/">WSO2 IoT Server</a>. </li><br>
<li>Unzip the WSO2 IoT Server distribution in any preferred location. This location is referred to as  <I>&lt;IOTS_HOME&gt;</I>  throughout this document. </li><br>
<li>Navigate to the <I>&lt;IOTS_HOME&gt;/bin</I> directory on a command prompt or terminal window, and execute the profile creator script. </li>

<strong>Linux/Mac OS</strong>
<table border="1" bgcolor="#D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
cd &lt;IOTS_HOME&gt;/bin
./profile-creator.sh
</pre>
</code>
</font></td></tr>
</table>
<strong>Windows</strong>
<table border="1" bgcolor="#D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
cd&lt;IOTS_HOME&gt;\bin
profile-creator.bat
</pre>
</code>
</font></td></tr>
</table>
<li>Enter the profile number of the profile you want to create. 
You can either enter the profile numbers separately to create multiple profiles or select the ‘All Profiles’ option (number 6) to create all the profiles in one go.</li><br>
<img src = "../../../image/WSO2-IoT-Server-profile-creator.png" style="border:2px solid black ">
<li>Once the profile number is entered, the profiles archive is created in the <I>target</I> directory. The <I>target</I> directory resides in the same directory as <I>&lt;IOTS_HOME&gt;</I> . Copy the respective archives into the preferred machines and locations.</li>
</ol>

<table border="1" bgcolor="#D9DEE1">
<tr><td><font style="Arial" size="2">
<strong>Multi-profiling</strong><br>
<strong>Tip:</strong> 
A particular profile contains only a subset of features bundled in the product. 
In order to identify what feature bundles apply to which profile, the product maintains a set of <a target="_blank" href="http://bundles.info/"> bundles.info</a> files. 
In WSO2 IoT Server, these files can be found in the <I>&lt;IOTS_HOME&gt;/wso2/components/&lt;profile-name&gt;/configuration/org.eclipse.equinox.simpleconfigurator</I> directories. 
The <a target="_blank" href="http://bundles.info/"> bundles.info</a> files contain references to the actual bundles.

Note that the &lt;profile-name&gt; in the directory path refers to the name of the profile. For example, when there's a product profile named device-backend, references to all the feature bundles required for the device-backend profile to function, are in a bundles.info file saved in the <I>&lt;IOTS_HOME&gt;/wso2/components/device-backend/configuration/org.eclipse.equinox.simpleconfigurator</I> directory.
</font></td></tr>
</table>