---
bookCollapseSection: true
weight: 1
---

# System Requirements

Prior to installing the product, it is necessary to have the appropriate prerequisite software installed on your system. Verify that the computer has the supported operating system and development platforms before starting the installation.

Entgra IoT Server comes as a composition of three components namely the broker, core, and analytics. All of these components are WSO2 Carbon based products. Following are the minimum system requirements to run all the IoT components.

<table border="1">
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">
        <p><strong>Memory</strong></p>
      </td>
      <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">
        <ul>
          <li>~ 8 GB minimum (16 GB is recommended for a production environment.)</li>
          <li>~ 1024 MB heap size. This is generally sufficient to process typical SOAP messages but the requirements vary with larger message sizes and&nbsp;the number of messages processed concurrently.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">
        <p><strong>Disk</strong></p>
      </td>
      <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">
        <ul>
          <li>~ 5 GB, excluding space allocated for log files, mobile applications, and databases.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2"><font style="Tahoma" size="2"><strong>Processor</strong></td>
      <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">
        <ul>
          <li><span>Quad core processor<br></span></li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

## Environment Compatibility

<table border="1">
  <colgroup>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr>
      <td><font style="Tahoma" size="2"><font style="Tahoma" size="2">
        <p><strong>Operating Systems</strong></p>
      </td>
      <td><font style="Tahoma" size="2">
        Product is extensively tested on linux based operating systems and also on Windows. For example on following,
        <ul style="list-style-type:disc;">
            <li>Ubuntu 18.04</li>
            <li>CentOS 7.6</li>
            <li>Windows Server 2016</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2">
        <p><strong>Databases</strong></p>
      </td>
      <td><font style="Tahoma" size="2">
        <ul>
          <li>All WSO2 Carbon-based products are generally compatible with most common DBMSs.
          We recommend you use an industry-standard RDBMS such as,
          <ul style="list-style-type:disc;">
            <li>Oracle</li>
            <li>PostgreSQL</li>
            <li>MySQL</li>
            <li>MS SQL, etc.</li>
          </ul>
          <br/>
          <li>Please also note the following guidelines,
            <ul style="list-style-type:disc;">
              <li>The embedded H2 database is not recommended for production environments</li>
              <li>It is not recommended to use Apache DS in a production environment due to scalability issues. Instead, use an LDAP like OpenLDAP for user management.</li>
            </ul>
          </li>
          <li>If you have difficulty in setting up any Entgra product on a specific platform or database,&nbsp;please <a href="http://entgra.io/contact" class="external-link" rel="nofollow">contact us</a>.</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

## Required Applications

The following applications are required for running the product.

<table border="1">
  <colgroup>
    <col>
    <col>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr bgcolor="#B8C4CC">
      <th align="center" width="200 px"><font style="Tahoma" size="2">
        <p>Application</p>
      </th>
      <th align="center" width="800 px"><font style="Tahoma" size="2">
        <p>Purpose</p>
      </th>
      <th align="center" width="50 px"><font style="Tahoma" size="2">
        <p>Version</p>
      </th>
      <th align="center" width="50 px"><font style="Tahoma" size="2">Download Links</th>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2">
        <p><strong>* Oracle Java SE Development Kit (JDK)</strong></p>
      </td>
      <td><font style="Tahoma" size="2">
        <ul>
          <li>All WSO2 Carbon-based products are Java applications that can be run on any platform that is JDK 8 compliant. Also, we <strong>do not recommend or support OpenJDK</strong>.</li>
        </ul>
      </td>
      <td><font style="Tahoma" size="2">
        <p>JDK 8</p>
      </td>
      <td><font style="Tahoma" size="2">
        <p><a rel="nofollow" href="http://java.sun.com/javase/downloads/index.jsp" class="external-link">http://java.sun.com/javase/downloads/index.jsp</a></p>
      </td>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2">
        <p><strong>Web Browser</strong></p>
      </td>
      <td><font style="Tahoma" size="2">
        <ul>
          <li>To access the product's <a href="{{< param doclink>}}product-guide/download-and-start-the-server/">Management Console</a>. The Web Browser must be JavaScript enabled to take full advantage of the Management console. Recommended Google Chrome.</li>
          <li><strong>NOTE:</strong> On Windows Server 2003, you must not go below the medium security level in Internet Explorer 6.x.</li>
        </ul>
      </td>
      <td><font style="Tahoma" size="2">
        <p><br></p>
      </td>
      <td><font style="Tahoma" size="2"><br></td>
    </tr>    
  </tbody>
</table>


## Optional Applications

The following applications are optional for running the product.

<table border="1">
  <colgroup>
    <col>
    <col>
    <col>
    <col>
  </colgroup>
  <tbody>
    <tr bgcolor="#B8C4CC">
      <th align="center"><font style="Tahoma" size="2">
        Application
      </th>
      <th align="center"><font style="Tahoma" size="2">
       Purpose
      </th>
      <th align="center"><font style="Tahoma" size="2">
        Version
      </th>
      <th align="center"><font style="Tahoma" size="2">Download Links</th>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2">
        <p><strong>Apache Maven</strong></p>
      </td>
      <td><font style="Tahoma" size="2">
        <p>Required to install plugins which are not packed with the product such as windows and</p>
      </td>
      <td><font style="Tahoma" size="2">
        <p>3.0.x</p>
      </td>
      <td><font style="Tahoma" size="2">
        <p><a href="http://maven.apache.org/" class="external-link" rel="nofollow">http://maven.apache.org/</a> &nbsp;</p>
      </td>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2"><strong>Git</strong></td>
      <td><font style="Tahoma" size="2">
        <p>To&nbsp;checkout the source, in order to <a href="https://wso2.github.io/" class="external-link" rel="nofollow">build the product from the source distribution</a>.</p>
      </td>
      <td><font style="Tahoma" size="2">1.8.*</td>
      <td><font style="Tahoma" size="2"><a href="http://git-scm.com/" class="external-link" rel="nofollow">http://git-scm.com/</a></td>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2">
        <p><strong>Web Browser</strong></p>
      </td>
      <td><font style="Tahoma" size="2">
        <ul>
          <li>To access the product's <a href="{{< param doclink>}}product-guide/download-and-start-the-server/" data-linked-resource-id="336566421" data-linked-resource-version="1" data-linked-resource-type="page">Management Console</a>. The Web Browser must be JavaScript enabled to take full advantage of the Management console.</li>
          <li><strong>NOTE:</strong> On Windows Server 2003, you must not go below the medium security level in Internet Explorer 6.x.</li>
        </ul>
      </td>
      <td><font style="Tahoma" size="2">
        <p><br></p>
      </td>
      <td><font style="Tahoma" size="2"><br></td>
    </tr>
  </tbody>
</table>
