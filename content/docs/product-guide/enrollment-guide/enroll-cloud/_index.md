---
bookCollapseSection: true
weight: 1
title: "Cloud Enrollment Guide"
---

# Cloud Enrollment Guide

The Entgra Evaluation Cloud is available for customers registered with Entgra. The Cloud Enrollment Guide is aimed at helping administrators with the types of enrollment options available.

If you are unsure about the type of enrollment that suits you best, going through the <a target=”_blank” href="https://entgra-documentation.gitlab.io/v4.0.0/docs/about-entgra-IoT-server/feature-list/">Feature List </a> for your device type might be helpful in deciding the best option for you.

Once you decide on the enrollment type, simply click on the video tutorial of the same following its prerequisites. 

Should you require any help along this, feel free to drop us a mail to <a href="mailto:bizdev-group@entgra.io">bizdev-group@entgra.io</a> with your registered mail and we will be in touch.

<br>

<table class="tableizer-table" border="1">
    <thead bgcolor="#B8C4CC">
        <tr class="tableizer-firstrow">
            <th><font style="Tahoma" size="2">Device Type</th>
            <th><font style="Tahoma" size="2">Enrollment Type</th>
            <th><font style="Tahoma" size="2">Prerequisites</th>
            <th><font style="Tahoma" size="2">Enrollment Video</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan="2" align="center"><font style="Tahoma" size="2"> <strong>Android</strong> <br><img src = "../../../image/logo_android_website.jpg" style="border:1px solid black "></td>
            <td align="left"><font style="Tahoma" size="2"> 
            1. Fully Managed - with <i>Device Setup Wizard</i> and QR Code <br> 
          </td>
            <td align="center"><font style="Tahoma" size="2"> new or factory-reset device </td>
            <td align="center"><font style="Tahoma" size="2"> <i>Fully Managed with QR</i></td>
        </tr>
         <tr><td align="left"><font style="Tahoma" size="2"> 
            2. Fully Managed for already booted device via QR Code
                         </td>
         <td align="center"><font style="Tahoma" size="2"> already booted </td>
        <td align="center"><font style="Tahoma" size="2"> <i>Already Booted with QR</i> </td>
        </tr>
        <tr>
        <td rowspan="2" align="center"><font style="Tahoma" size="2"> <strong>iOS</strong> <br><img src = "../../../image/logo_iOS_website.jpg" style="border:1px solid black "></td>
        <td align="left"><font style="Tahoma" size="2"> 
        1. 
        </td>
        <td align="center"><font style="Tahoma" size="2">  </td>
        <td align="center"><font style="Tahoma" size="2"> <i></i></td>
        </tr>
       <tr><td align="left"><font style="Tahoma" size="2"> 
        2. 
        </td>
         <td align="center"><font style="Tahoma" size="2">  </td>
         <td align="center"><font style="Tahoma" size="2"> <i></i> </td>
         </tr>
         <tr>
          <td rowspan="2" align="center"><font style="Tahoma" size="2"> <strong>Windows</strong> <br><img src = "../../../image/logo_windows_website.jpg" style="border:1px solid black "></td>
           <td align="left"><font style="Tahoma" size="2"> 
           1.  
           </td>
           <td align="center"><font style="Tahoma" size="2"> </td>
            <td align="center"><font style="Tahoma" size="2"> <i></i></td>
            </tr>
             <tr><td align="left"><font style="Tahoma" size="2"> 
              2. 
              </td>
              <td align="center"><font style="Tahoma" size="2">  </td>
              <td align="center"><font style="Tahoma" size="2"> <i></i> </td>
         </tr>
          </tbody>
        </table>
