---
bookCollapseSection: true
weight: 1
---

# Android Device Enrollment as a Dedicated Device using QR Code

{{< hint info >}}
<strong>Prerequisites</strong><br>
<ul style="list-style-type:disc;">
<li>Server has to be <a href="{{< param doclink >}}product-guide/download-and-start-the-server/">downloaded and started</a>.</li>
 <li>Must have been logged on to the server's <a href="{{< param doclink >}}product-guide/login-guide/">Device Management Portal</a>.</li>
<li>Optionally, <a href="{{< param doclink >}}key-concepts/#android ">Basic Concepts of Android Device Management</a> will be beneficial as well. </li>
<li>When enrolling a device using QR Code, the the <a  href="{{< param doclink >}}product-guide/enrollment-guide/enroll-android/android-configurations/"> Platform Configurations </a> for an Android device should have been first set.</li> 
 </ul>
{{< /   hint >}}



<iframe width="560" height="315" src="https://www.youtube.com/embed/jWaFF5p_epw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Set platform configurations as indicated in the prerequisties.</li>
    <li>Click <strong>Enroll Device</strong> and select the device type.</li>
    <li>In the server, select the device ownership as <strong>COSU (KIOSK)</strong>.</li>
    <li>Tap 6 times on the welcome screen after factory reset. <strong>Note:</strong> This only works for devices on and above android 9.0.</li>
    <li>Scan the QR code that is generated on the server.</li>
    <li>Read the license agreement and the privacy policy and then click <strong>Next</strong> to proceed.</li>
</ul>
