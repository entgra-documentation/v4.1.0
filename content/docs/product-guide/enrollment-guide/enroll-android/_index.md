---
bookCollapseSection: true
weight: 2
---

# Enrolling Android Devices

This section describes the types of enrollment available for Android Devices. Prior to this section, make sure to follow the  [Installing Agent]({{< param doclink >}}product-guide/enrollment-guide/enroll-android/install-agent/) section.
The available enrollment types are:

 1. [Work Profile - Manual Enrollment]({{< param doclink >}}product-guide/enrollment-guide/enroll-android/enroll-android-as-work-profile/enroll-android-as-work-profile-manually/)
 2. [Work Profile - Using QR Code]({{< param doclink >}}product-guide/enrollment-guide/enroll-android/enroll-android-as-work-profile/enroll-android-as-work-profile-using-QR-code/)
 3. [Fully Managed Device - Manually]({{< param doclink >}}product-guide/enrollment-guide/enroll-android/enroll-android-as-a-fully-managed-device/enroll-android-as-a-fully-managed-device-manually/)
 4. [Fully Managed Device - Using QR Code]({{< param doclink >}}product-guide/enrollment-guide/enroll-android/enroll-android-as-a-fully-managed-device/enroll-android-as-a-fully-managed-device-using-QR-code/)
 5. [Dedicated Device - Using QR Code]({{< param doclink >}}product-guide/enrollment-guide/enroll-android/enroll-android-as-a-dedicated-device/enroll-android-as-a-dedicated-device-using-QR-code/)
 6. [Dedicated Device with Entgra Agent - Using QR Code]({{< param doclink >}}product-guide/enrollment-guide/enroll-android/enroll-android-as-a-dedicated-device/enroll-android-as-a-dedicated-device-with-entgra-agent-using-QR-code/)
 7. [Legacy Enrollment - Using QR Code]({{< param doclink >}}product-guide/enrollment-guide/enroll-android/enroll-android-as-a-legacy-device/legacy-enrollment-using-qr-code/)
 8. [Legacy Enrollment - Manually]({{< param doclink >}}product-guide/enrollment-guide/enroll-android/enroll-android-as-a-legacy-device/legacy-enrollment-manually/)
