---
bookCollapseSection: true
weight: 2
title: "Product Guide"
---

# Working with the Product

The Product Guide section is aimed at guiding you with step-by-step procedures and videos on working with the Entgra IoT server. Covered within this, is first the <a href="system-requirements">System Requirements</a> outlining the required specifications for running Entgra IoT in your system.

<a href="download-and-start-the-server">Downloading and starting</a> the Entgra IoT server is covered next, followed by the  <a href="login-guide">Log-In Guide</a> section which illustrates the correct way to log in to the server.

Complete guides on enrolling Android, iOS, macOS and Windows devices are covered in the <a href="enrollment-guide">Enrollment Guide</a> section. These include procedures for all types of enrollment available for iOS devices including the standard DEP enrollment, BYOD along with additional server configurations for each.  For Android devices, detailed procedures with and without using the QR code, including that of using the Entgra Agent are covered within this section. Additional Server configurations for Windows enrollments have been provided along with steps for enrolling Windows devices.

The <a href="device-management-guide">Device Management Guide</a> section takes you through instructions on how to manage an enrolled device, the required general platform configurations and detailed coverage on how operations and policies are applicable to each Android, Apple and/or Windows device type. 

Managing a multi-tenant system whilst achieving maximum resource sharing across multiple users ensuring optimal performance is explained in detail under the <a href="manage-tenants">Manage Tenant</a> section. 

A segment on how to assign and manage Roles and permissions is given under <a href="manage-roles">Manage Roles</a>. This is followed up by the user management section covered under <a href="manage-users">Manage Users</a>. 

The <a href="app-management">App Management</a> section takes you through on how to publish a new app, its releases and lifecycle management. This also covers <a href="app-management/google-enterprise-app-management/">Google Enterprise App Management</a>  and also steps on how to <a href="app-management/install-and-uninstall-app/">Install and Uninstall Apps</a>. 

The <a href="../key-concepts">Key Concepts</a> section aims to take you through the main concepts used within and applied to in Mobile Device Management. Brief introductions to the concepts and terminology used will enable easier understanding of the applicable use cases. 

Descriptive explanations of each of the sections are given below:

<div>
<ul style="list-style-type:disc;">
<li><a href="system-requirements">System Requirements</a></li>
<li><a href="download-and-start-the-server">Downloading and Starting the Server</a></li>
<li><a href="login-guide">Log-In Guide</a></li>
<li><a href="enrollment-guide">Enrollment Guide</a></li>
<li><a href="device-management-guide">Device Management Guide</a></li>
<li><a href="manage-tenants">Manage Tenants</a></li>
<li><a href="manage-roles">Manage Roles</a></li>
<li><a href="manage-users">Manage Users</a></li>
<li><a href="app-management">App Management</a></li>
<li><a href="../key-concepts">Key Concepts</a></li>
</ul>
</div>