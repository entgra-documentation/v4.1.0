---
bookCollapseSection: true
weight: 2
---

# Restrictions

{{< hint info >}}
To add a new policy, follow instructions as given in <b> <a href ="{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/#add-a-policy"> Add Policy </a></b> under Policy Descriptions for Android Devices. 
{{< /hint >}}

The Restrictions Policy for Android devices enables restricting specific settings on the device. Once the profile with these configurations is installed on the device, the user is unable to modify those settings on his/her device.

<strong> Disable/Block Configuring Credentials </strong>

This configuration blocks the user from being able to configure credentials in the security certificates. 
In order to test this under Work Profile enrollment, enforce this policy on the device. Then go to device settings.

<ol>
<li>Search for certificates, and click <strong>View Security Certificates</strong>.</li>


<img src ="configuringCredentials.png" style="border:5px solid black ">

<li>If you tap on certificates under personal, a pop-up will appear allowing you to turn off a certificate.</li>

<img src ="configuringCredentials2.png" style="border:5px solid black ">

<li> Since the policy is applied under Work Profile enrollment, the turn-off button is disabled/not visible.</li>

<img src ="configuringCredentials3.png" style="border:5px solid black ">
</ol>

<strong> Disable/Block Configuring VPN </strong>

This restriction blocks configuring VPN settings. To test this under Work Profile enrollment, download a free VPN app, for example “turbo VPN” from play store and try to create a VPN connection. The application will show an error.

<strong> Disable/Block Configuring App Control </strong> 

Specifies if a user is blocked from modifying applications in Settings or launchers. The following actions will not be allowed for the user when this restriction is enforced:


<li>Uninstalling Apps</li>

<li>Disabling Apps</li>

<li>Clearing App caches</li>

<li>Clearing App Data</li>

<li>Force Stopping Apps</li>

<li>Clearing App Defaults</li>

After applying the policy, try to remove an installed app, for example the VPN client from the device, and it will not work.

<img src ="DisallowConfiguringAppontrol.png" style="border:5px solid black ">

In the first image, uninstall is disabled, and the second image is from Settings -> Apps -> Installed App. Click **Uninstall** or **Force Stop**, or under application’s storage, click **Clear Data** or **Clear Cache**. The policy will prevent you from doing so.

<strong> Disable/Block Cross Profile Copy-Paste </strong>


This feature disables text copy-pasting between Work Profile and the regular profile. To test how this works, you can go to an app where you can type text. For example, in the Google Playstore’s search box. Type some text and copy it. Next, try pasting the copied text into another app on the phone, that is not a part of the Work Profile container. The text will not be available. Remove the policy and retry.

<strong> Disable/Block Debugging </strong>

This feature blocks the ability to debug any application through the Android studio.

<strong> Disable Installing Apps </strong>

Try to install an app using google play store on the work profile. This will not be allowed and  

<strong> Disable Installing from Unknown Sources </strong>

Install a browser such as chrome in the work profile and try to install an apk file from untrusted source such as

 https://github.com/selendroid/selendroid/raw/master/selendroid-standalone/src/test/resources/selendroid-test-app.apk 

This will be prevented by the OS.

<strong> Disable Modifying Accounts</strong>

Go to Settings -> “Cloud and accounts” -> “accounts”
Under work, the add account will be disabled. Also if you go inside the existing Google account of the work profile and click on the 3 dot menu on the top right corner, the remove button is greyed out(Disabled). 

<strong>Disable Outgoing Beams</strong>

This is related to NFC beams. Install an NFC app that sends some information to another. for example NFC contacts application into the work profile and try to send NFC beam to another NFC enabled device.

<strong>Disable Location Sharing</strong>

This restriction disables turning on location sharing.
Under settings -> location  -> the work profile and regular profiles location control is available.

<strong>Disable Uninstalling Apps</strong>

After enabling this restriction, install an app through the work profile and try to uninstall it and it is not allowed.

<strong> Ensure verifying apps </strong>

In settings, go to Google -> Work -> Security -> Verify apps -> Scan device for security threats is not configurable.

<strong> Enable auto timing </strong>

Search for automatic date and time in settings of the device and enabling/disabling this setting is controlled by this setting.

<strong> Disable screen capture </strong>

This will disable the ability to take screenshots by pressing the power key together with volume down.

{{< hint info >}}
Following set of restrictions require the device to be in device owner mode.
Follow the <b><a href ="https://entgra-documentation.gitlab.io/v3.7
.0/docs/guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> documentation </a></b> to get the device into device owner mode.
{{< /hint >}}

<strong>Disable SMS</strong>

The user will not be allowed to send or receive SMS.

<strong>Disable Volume Adjust</strong>

This will disable the ability changing device volume. so the volume keys must not have an effect.

<strong>Disable Cell Broadcast</strong>

Disables cell broadcasting messages (message -> Settings -> Cell Broadcast).

<strong>Disable Configuring Bluetooth</strong>

This restriction will disable the Bluetooth settings in settings of the device.

<strong>Disable Configuring Mobile Networks</strong>

Under settings -> mobile networks -> mobile configurations menu disabling is achieved with this configuration.

<strong>Disable Configuring Tethering</strong>

This will disable the ability to configure mobile hotspots and tethering which is found in settings.

<strong>Disable Configuring WiFi</strong>

Disable the ability to configure wifi settings in device settings.

<strong>Disable Safe Boot</strong>

Disables the ability to safe boot a device to remove any apps installed.
https://support.t-mobile.com/docs/DOC-34283

<strong>Disable Outgoing Calls</strong>

The user is not allowed to make outgoing phone calls. Emergency calls are still permitted.

<strong>Disable Mounting Physical Media</strong>

The user is disableed from mounting physical external media. Connect a Pendrive to the device via the OTG cable and the device will not allowed to mount Pendrive.

<strong>Disable Creating Window</strong>

Apps running are not allowed to create following <a href ="https://developer.android.com/reference/android/os/UserManager.html#DISALLOW_CREATE_WINDOWS"> types </a></b> of windows.

<ul>
    <li>LayoutParams#TYPE_TOAST</li>
    <li>LayoutParams#TYPE_PHONE</li>
    <li>LayoutParams#TYPE_PRIORITY_PHONE</li>
    <li>LayoutParams#TYPE_SYSTEM_ALERT</li>
    <li>LayoutParams#TYPE_SYSTEM_ERROR</li>
    <li>LayoutParams#TYPE_SYSTEM_OVERLAY</li>
    <li>LayoutParams#TYPE_APPLICATION_OVERLAY</li>
</ul>

To test, have an application that creates a toast and for example in the VPN app, when the VPN is created a toast may be shown and this must be Disableed with this setting.

<img src ="DisableCreteWindow.png" style="border:5px solid black ">

<strong>Disable Factory Reset</strong>

Disabled the ability to factory set the device. Go to settings on the device, and the factory reset must be disabled.

<strong>Disable Remove User / Disable Add User </strong>

Multiple user profile control. The users are not allowed to remove or add. This may not be <a href ="https://support.google.com/nexus/answer/2865483?hl=en"> available </a></b> in some Android devices.

<strong> Disable Network Reset </strong>

This restriction will disable network resetting and to check, type "reset network settings" in the settings search bar and the reset settings will be disabled.

<strong>Disable USB File Transfer</strong>

This restriction will disable the file transfer via USB.

<strong>Disable Factory Reset</strong>

Disabled the ability to factory set the device. Go to settings on the device, and the factory reset must be disabled.

<strong>Disable Unmute Microphone</strong>

This restriction will disable the microphone. Check the device microphone by using a recording app.

<strong>Disable status bar </strong>

This restriction will disable the device status bar.

<img src ="statusBar.png" style="border:5px solid black ">

<strong> Disable Autofill </strong>

This restriction will disable autofill services.To check, install a application that needs to enter user credintials to log in. when entering the credintials, device autofill service will ask to save credincials to autofill service (samsung devices uses samsung pass as default).After when this restiriction active,this auto fill service will be disabled.

<img src ="autoFill.jpg" style="border:5px solid black width:500px height:600px">

 {{< hint info >}}
To publish a new policy, refer to <b> <a href = "{{< param doclink >}}product-guide/device-management-guide/android-devices/android-device-policies/#publish-a-policy"> Publish a Policy </a></b> under policy descriptions for Android devices.
 {{< /hint >}}