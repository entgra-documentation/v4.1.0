---
bookCollapseSection: true
weight: 13
---

# Network Usage Rules

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}
 
Network Usage Rules allow enterprises to specify how managed apps use networks, such as cellular data networks.

<i>These rules only apply to managed apps.</i>


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Allow cellular data when roaming</strong>
                <br><i>(Common to all rule 
            configuration types)</i></td>
            <td>If set to false, matching managed apps will not be allowed to use cellular data when roaming.</td>
        </tr>
        <tr>
            <td><strong>Allow Cellular Data</strong>
                <br><i>(Common to all rule 
                                                                configuration types)</i></td>
            <td> If set to false, matching managed apps will not be allowed to use cellular data at any time.
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <center><strong>Applly to specified managed apps</strong>
                    <br>(Set network usage rules to specific applications)
                </center>
            </td>
        </tr>
        <tr>
            <td><strong>Application Identifier Match</strong></td>
            <td>A list of managed app identifiers, as strings, that must follow the associated rules. If this key is missing, the rules will apply to all managed apps on the device
                <br>Each string in the Application Identifier Match may either be an exact app identifier match,
                <br><i>[e.g . com.mycompany.myapp]</i>
                <br>or it may specify a prefix match for the Bundle ID by using the * wildcard character. The wildcard character, if used, must appear after a period character (.), and may only appear once, at the end of the string
                <br><i>[e.g. com .mycompany .*.]</i></td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}
